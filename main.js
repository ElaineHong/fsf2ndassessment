
// import mysql module from node_modules.
var mysql = require("mysql");
var path = require("path");
var express = require("express");

// create a connection pool passing in connection info.
var pool = mysql.createPool({
    host: "localhost",
    port: "3306",
    user: "elainehong",
    password: "appdeveloper88",
    database: "books",
    connectionLimit: 4
});

const  PORT = "port";
const GET_ALL_BOOKS = "select cover_thumbnail, title, author_lastname, author_firstname" +
    "from books";

const GET_ALL_BOOKS_WHERE_TITLE_LIKE_AS = "select cover_thumbnail, title, author_lastname, author_firstname" +
    "from books where title like ?";

// instantiate the express module to app object.
var app = express();

app.get("/api/books/:author_lastname", function(req, res){
    console.info(req);
    console.info(req.params.author_lastname);
    pool.getConnection(function(err, connection) {
        if(err){
            res.status(400).send(JSON.stringify(err));
            return;
        }
        connection.query("select * from books where author_lastname=?", [req.params.author_lastname],
            function(err, results){
                connection.release();
                if(err){
                    res.status(400).send(JSON.stringify(err));
                    return;
                }
                console.info(results);
                res.json(results[0]);
            }
        );
    });
});


app.get("/api/books/title/:title_like", function(req, res){
    console.info(req);


    pool.getConnection(function(err, connection){
        if(err){
            res.status(400).send(JSON.stringify(err));
            return;
        }
        var title_like = "%" + req.params.title_like + "%";
        connection.query(GET_ALL_BOOKS_WHERE_TITLE_LIKE_AS,[title_like],function(err, results){
            connection.release();
            if(err){
                res.status(400).send(JSON.stringify(err));
                return;
            }
            console.info(results);
            res.json(results);
        });
    });
});


app.get("/api/books", function(req, res){
    console.info(req);
    pool.getConnection(function(err, connection){
        if(err){
            res.status(400).send(JSON.stringify(err));
            return;
        }
        connection.query(GET_ALL_BOOKS,[],function(err, results){
            connection.release();
            if(err){
                res.status(400).send(JSON.stringify(err));
                return;
            }
            console.info(results);
            res.json(results);
        });
    });
});


app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT) , function(){
    console.info("App Server started on " + app.get(PORT));
});