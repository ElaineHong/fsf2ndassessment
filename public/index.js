
//The angular Set-up
(function(){
angular
.module ("QueryApp",[])
.controller("queryCtrl",queryCtrl);

queryCtrl.$inject=["$http"];

//the queryCtrl Controller

function queryCtrl($http){
    var vm = this;
    vm.author_lastname = "";
    vm.result=null;
    vm.search=function(){
        $http.get("/api/books/" + vm.author_lastname)
            .then(function(result){
                vm.result=result.data;
                console.info("result:%s", JSON.stringify(vm.result));
            })
            .catch(function(error){
                console.info(">>error:%s",JSON.stringify(error));
            });
    }


}

})();